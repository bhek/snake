from curses import *
from curses import panel
from random import randint

grid = [' ################################ ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' #                              # ',
        ' ################################ ']

player = '$'
point = '.'
score = 0

player_x = 14
player_y = 6

point_x = randint(2, 31)
point_y = randint(2, 13)

def draw_board(win):
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            tile = grid[i][j]
            win.attron(color_pair(1))
            win.addstr(i, j, tile)
            win.attroff(color_pair(1))

def point_check(panel, x, y):
    global point_x, point_y
    if x == point_x and y == point_y:
        new = False
        while(not new):
            new_x = randint(2, 31)
            new_y = randint(2, 13)
            if new_x != x and new_y != y:
                point_x = new_x
                point_y = new_y
                panel.move(point_y, point_x)
                global score
                score += 5
                new = True

def move(panel, x, y):
    global player_x, player_y
    player_x = x
    player_y = y
    panel.move(player_y, player_x)

def main():
    stdscr = initscr()
    start_color()
    noecho()
    curs_set(False)
    stdscr.keypad(True)
    stdscr.nodelay(True)    # stops getch() from blocking the program

    init_pair(1, COLOR_RED, COLOR_BLACK)
    init_pair(2, COLOR_GREEN, COLOR_BLACK)

    board_window = newwin(20, 40, 0, 0)
    draw_board(board_window)
    board_panel = panel.new_panel(board_window)

    player_win = newwin(1, 1, player_y, player_x)
    player_win.bkgd(player, color_pair(2))
    player_panel = panel.new_panel(player_win)

    point_win = newwin(1, 1, point_y, point_x)
    point_win.bkgd(point)
    point_panel = panel.new_panel(point_win)

    panel.update_panels()
    doupdate()

    direction = 'r'
    game = True
    while(game):
        napms(250)
        key = stdscr.getch()
        if key == 27:
            game = False
            break
        elif key == KEY_RIGHT:
            point_check(point_panel, player_x + 1, player_y)
            move(player_panel, player_x + 1, player_y)
            direction = 'r'
        elif key == KEY_LEFT:
            point_check(point_panel, player_x - 1, player_y)
            move(player_panel, player_x - 1, player_y)
            direction = 'l'
        elif key == KEY_UP:
            point_check(point_panel, player_x, player_y - 1)
            move(player_panel, player_x, player_y - 1)
            direction = 'u'
        elif key == KEY_DOWN:
            point_check(point_panel, player_x, player_y + 1)
            move(player_panel, player_x, player_y + 1)
            direction = 'd'
        else:
            if direction == 'r':
                point_check(point_panel, player_x + 1, player_y)
                move(player_panel, player_x + 1, player_y)
                direction = 'r'
            elif direction == 'l':
                point_check(point_panel, player_x - 1, player_y)
                move(player_panel, player_x - 1, player_y)
                direction = 'l'
            elif direction == 'u':
                point_check(point_panel, player_x, player_y - 1)
                move(player_panel, player_x, player_y - 1)
                direction = 'u'
            elif direction == 'd':
                point_check(point_panel, player_x, player_y + 1)
                move(player_panel, player_x, player_y + 1)
                direction = 'd'

        if grid[player_y][player_x] == '#':
            game = False
 
        panel.update_panels()
        doupdate()

    while (not game):
        stdscr.addstr(5, 34, "You lose")
        stdscr.addstr(6, 34, "Score: " + str(score))
        stdscr.addstr(7, 34, "Press Esc to quit")
        key = stdscr.getch()
        if key == 27:
            game = True
            break
    endwin()

if __name__ == "__main__":
    main()
